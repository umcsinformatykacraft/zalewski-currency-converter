package pl.zemoz;

import java.util.Objects;

public class Money {
    private final Double amount;
    private final Currency currency;

    public Money(Double amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return Objects.equals(getAmount(), money.getAmount()) &&
                Objects.equals(getCurrency(), money.getCurrency());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getAmount(), getCurrency());
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency=" + currency +
                '}';
    }
}
