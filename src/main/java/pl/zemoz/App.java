package pl.zemoz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class App
{
    public static void main( String[] args )
    {
        CurrencyConverter currencyConverter = new CurrencyConverter();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<ExchangeRate> ex = new ArrayList<ExchangeRate>(3);
        try {
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-01"), new Currency("USD"), 3.0000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-01"), new Currency("GBP"), 5.0000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-02"), new Currency("USD"), 3.5000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-04"), new Currency("USD"), 3.1000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-03"), new Currency("USD"), 3.2000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-07"), new Currency("USD"), 3.3000));
            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-01-12"), new Currency("USD"), 3.4000));

            ex.add(new ExchangeRate(simpleDateFormat.parse("2019-11-12"), new Currency("USD"), 10.4000));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ex.forEach(currencyConverter::provideExchangeRate);

        Money money = currencyConverter.exchange(new Money(10.0, new Currency("USD")), new Currency("GBP"), new Date());

        System.out.println(money);
    }
}
