package pl.zemoz;

import java.util.Date;
import java.util.Objects;

public class ExchangeRate implements Comparable<ExchangeRate> {
    private final Date timestamp;
    private final Currency currency;
    private final Double baseRate;

    public ExchangeRate(Date timestamp, Currency currency, Double baseRate) {
        this.timestamp = timestamp;
        this.currency = currency;
        this.baseRate = baseRate;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Double getBaseRate() {
        return baseRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExchangeRate that = (ExchangeRate) o;
        return Objects.equals(getTimestamp(), that.getTimestamp()) &&
                Objects.equals(getCurrency(), that.getCurrency()) &&
                Objects.equals(getBaseRate(), that.getBaseRate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getTimestamp(), getCurrency(), getBaseRate());
    }

    @Override
    public String toString() {
        return "ExchangeRate{" +
                "timestamp=" + timestamp +
                ", currency=" + currency +
                ", baseRate=" + baseRate +
                '}';
    }

    @Override
    public int compareTo(ExchangeRate o) {
        return this.timestamp.compareTo(o.timestamp);
    }
}
