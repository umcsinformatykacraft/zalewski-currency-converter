package pl.zemoz;

import java.util.*;

public class CurrencyConverter {
    Map<Currency, SortedSet<ExchangeRate>> exchangeRates;

    public CurrencyConverter() {
        this.exchangeRates = new HashMap<>();
    }

    public void provideExchangeRate(ExchangeRate exchangeRate) {
        SortedSet<ExchangeRate> rates = this.exchangeRates.get(exchangeRate.getCurrency());
        if(rates == null) {
            rates = new TreeSet<>(Collections.reverseOrder());
        }
        if(!rates.add(exchangeRate)) {
            System.out.println(exchangeRate);
        }
        exchangeRates.put(exchangeRate.getCurrency(), rates);
    }

    public Money exchange(Money money, Currency targetCurrency, Date timestamp) {
        Currency currency = money.getCurrency();
        if(currency.equals(targetCurrency)) {
            return money;
        }

        ExchangeRate exchangeRateToBaseCurrency = findExchangeRate(timestamp, currency);
        Double baseAmount = money.getAmount() / exchangeRateToBaseCurrency.getBaseRate();

        ExchangeRate exchangeRateToTargetCurrency = findExchangeRate(timestamp, targetCurrency);
        Double targetAmount = baseAmount * exchangeRateToTargetCurrency.getBaseRate();
        return new Money(targetAmount, targetCurrency);
    }

    private ExchangeRate findExchangeRate(Date timestamp, Currency currency) {
        SortedSet<ExchangeRate> rates = this.exchangeRates.get(currency);
        for (ExchangeRate rate: rates) {
            if(timestamp.after(rate.getTimestamp())) {
                return rate;
            }
        }

        throw new RuntimeException("NO rate");
    }
}
